"""Long-term tasks somehow related to tracks."""
# from database.connection import session_scope
# from music_search.application import spotify_proxy
# from music_search.models.playlists import PlaylistModel  # noqa
# from music_search.models.tracks import Track
import threading
import time

from suggar_core.settings import Settings

from config import config

settings = Settings(config)
settings.setup(config)


def cache_search_results(tracks):
    """Cache tracks in database."""
    tracks_controller = settings.tracks_controller
    tracks_controller.create_many(tracks)


def retrieve_new_releases():
    """Retrieve just-released tracks from spotify."""
    def format_spotify_track(spotify_track):
        track = {
            'track_id': spotify_track['id'],
            'title': spotify_track['name'],
            'artist': ', '.join([
                artist['name']
                for artist in spotify_track['artists']
            ]),
            'meta': {
                'album': spotify_track['album'],
                'release_date': spotify_track['release_date']
            },
            'preview_url': spotify_track.get('preview_url')
        }
        print(track)
        return track

    tracks_controller = settings.tracks_controller
    new_tracks = settings.spotify_proxy.get_new_tracks()

    tracks = list(map(format_spotify_track, new_tracks))
    tracks_controller.create_many(tracks)

    return tracks


def _fetch_tracks(track_ids, tracks_features, fields):
    features_chunk = settings.spotify_proxy.get_track_feature(track_ids)
    for track_features in features_chunk:
        if not track_features:
            continue

        current_id = track_features['id']

        tracks_features[current_id] = {}
        for feature_name, feature_value in track_features.items():
            if feature_name in fields:
                tracks_features[current_id][feature_name] = feature_value


def collect_tracks_meta(limit=None):
    """Collect meta data for cached tracks."""
    tracks_controller = settings.tracks_controller
    tracks = tracks_controller.get_tracks(count=limit)

    tracks_ids = [
        track['track_id']
        for track in tracks]

    chunk_size = 20
    chunked_tracks_ids = [
        tracks_ids[i: i + chunk_size]
        for i in range(0, len(tracks_ids), chunk_size)]

    fields = (
        'danceability', 'energy', 'loudness', 'speechiness',
        'acousticness', 'instrumentalness', 'liveness', 'tempo')

    print('Begin fetching tracks features ({} tracks loaded)'
          .format(len(tracks_ids)))
    start_time = time.time()

    tracks_features = {}
    threads = []
    for ids_chunk in chunked_tracks_ids:
        current_thread = threading.Thread(
            target=_fetch_tracks,
            args=(ids_chunk, tracks_features, fields))
        threads.append(current_thread)
        current_thread.start()

    for thread in threads:
        thread.join()

    time_elapsed = time.time() - start_time
    print('Fetch completed. Time spent: {}'.format(time_elapsed))

    prepared_tracks = {}
    for track_id, track_features in tracks_features.items():
        prepared_tracks[track_id] = {
            'track_id': track_id,
            'meta': track_features
        }

    tracks_controller.update_many(prepared_tracks)

    return tracks_features
