import logging

from tasks import cache_search_results
from tasks import collect_tracks_meta
from tasks import retrieve_new_releases


logger = logging.getLogger()


def lambda_retrieve_new_releases(event, context):
    logger.info('Retrieving new releases')
    return retrieve_new_releases()


def lambda_collect_tracks_meta(event, context):
    logger.info('Collecting tracks meta...')
    return collect_tracks_meta()


def lambda_cache_search_results(event, context):
    tracks = event.get('tracks', [])
    logger.info('Caching search result. Data:')
    logger.info(event)

    return cache_search_results(tracks)
