#!/usr/bin/env bash

# If dir exists
if [ -d "tmp" ]; then
    rm -rf tmp
fi

mkdir tmp
export PYTHONPATH=tmp
echo "Copying project files"
rsync -av  --progress * tmp/ --exclude tmp >/dev/null

echo "Installing project dependencies"
pip3 install -t tmp/ testresources >/dev/null
pip3 install -t tmp/ -r requirements.txt --upgrade >/dev/null

echo "Packing stuff"
cd tmp
zip -r ../deploy_pack.zip . >/dev/null
cd ..

echo "Cleaning up"
rm -rf tmp

echo "Done."
