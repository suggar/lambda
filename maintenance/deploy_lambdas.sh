#!/usr/bin/env bash

export DEPLOYPACK_NAME="deploy_pack.zip"
export ZIP_PATH="$(pwd)/$DEPLOYPACK_NAME"

if [ ! -f $DEPLOYPACK_NAME ]; then
    echo "No deploy pack found. Aborting..."
    exit 1
fi

export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
source ~/.local/bin/virtualenvwrapper.sh
workon music_search

aws lambda update-function-code \
--function-name RetrieveNewTracks \
--zip-file fileb://$ZIP_PATH | jq '"Updated function \(.FunctionName). Current revision: \(.RevisionId)"'

aws lambda update-function-code \
--function-name CollectTracksMeta \
--zip-file fileb://$ZIP_PATH | jq '"Updated function \(.FunctionName). Current revision: \(.RevisionId)"'

aws lambda update-function-code \
--function-name CacheSearchResults \
--zip-file fileb://$ZIP_PATH | jq '"Updated function \(.FunctionName). Current revision: \(.RevisionId)"'

echo "Cleaning up here"
rm $DEPLOYPACK_NAME
