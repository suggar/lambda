"""App configuration."""
import os

from suggar_core.config import CoreConfigDefault


class _ConfigDefault(CoreConfigDefault):
    BASEDIR = os.path.abspath(os.path.dirname(__file__))
    DEBUG = False

    CORE_DB_URI = os.environ.get('DB_URI')
    CORE_IS_TESTING = False

    CORE_SPOTIFY_CLIENT_ID = os.environ.get('SPOTIFY_CLIENTID')
    CORE_SPOTIFY_SECRET = os.environ.get('SPOTIFY_SECRET')
    CORE_SPOTIFY_REDIRECT_URI = os.environ.get('SPOTIFY_REDIRECT_URI')

    AWS_KEYID = os.environ.get('AWS_KEYID')
    AWS_SECRET_KEY = os.environ.get('AWS_SECRET_KEY')
    AWS_REGION = os.environ.get('AWS_REGION')

    CORE_SERVER_SECRET = os.environ.get('SERVER_SECRET')

    LAMBDAS = {
        'cache_search_results':
            'arn:aws:sns:us-east-2:293192558952:search_caching'}


class _ConfigProd(_ConfigDefault):
    pass


class _ConfigDev(_ConfigDefault):
    DEBUG = True


class _ConfigTest(_ConfigDefault):
    CORE_IS_TESTING = True


basedir = ''

_env = os.environ.get('ENV', 'PROD')
_configs = {
    'PROD': _ConfigProd,
    'DEV': _ConfigDev,
    'TEST': _ConfigTest}

config = _configs[_env]
