"""Module with app initialization."""
from suggar_core.settings import Settings

from config import config


settings = Settings(config)
if not config.CORE_IS_TESTING:
    settings.setup(config)
